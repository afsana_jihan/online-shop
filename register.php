<?php include("Database.php"); ?>
<?php
$databaseOb=new Database();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $obDatabase = new Database();
    $obDatabase->insertNewUserToUserTable($_POST['name'],$_POST['email'],$_POST['phone'],$_POST['password']);
    header("Location: index.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php include("head.php");?>
			<?php include("socialmanage.php");?>
			</div>
			<div class="container">
				<div class="row menu">
					<?php include("option.php");?>
					<?php include("addToCart.php");?>
				</div>
			</div>
		</section>
		<section class="breadcrums">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 brd-1">
					<h2>Register</h2>
				</div>
			</div>
		
		</div>
		
		</section>
		<section class="login-page">
			<div class="container">
			<div class="col-md-offset-4 col-md-5">
				<div class="login">
				<h2>Register</h2>
				   <form  method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
						<span>Name:</span></br><input type="text" value="" name="name" required> </br>
						
						<span>Email:</span></br><input type="text" value="" name="email" required> </br>
						<span> Phone Number:</span></br><input type="number"  name="phone" required></br>
						<span>Password:</span></br>
						<input type="password" id="password" name="password"  required><br>
						<span> Confirm-Password:</span></br>
						<input type="password" id="confirmPassword" name="confirmPassword"  required oninput="check(this)"><br>
					
						<script language='javascript' type='text/javascript'>
                    function check(input) {
                        if (input.value != document.getElementById('password').value) {
                            input.setCustomValidity('Password Must be Matching.');
                        } else {
                            input.setCustomValidity('');
                        }
                    }
                </script>
						<a href=""><button>Submit</button></a>
						
					</form>
				</div>
		</div>
			</div>
			</div>
		</section>
		
		
		
		
		
		
		
		
		
			<?php include("websiteEndBody.php");?>
						
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/jquery.validate.js"></script
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>



</body>
</html>