$(document).ready(function(){
	var owl=$('.slides');
	owl.owlCarousel({
		items:1,
		autoplay:true,
		autoplayHoverPause:false,
		autoplayTimeout:3000,
		dots:true,
		nav:false,
		loop:true,
		margin:0,
	});
	
	var owl=$('.testimonials');
	owl.owlCarousel({
		items:1,
		autoplay:true,
		autoplayHoverPause:false,
		autoplayTimeout:3000,
		dots:true,
		nav:false,
		loop:true,
		margin:0,
	});
	
$('.portfolio').magnificPopup({
  delegate: 'a', // child items selector, by clicking on it popup will open
  type: 'image'
  // other options
});
});