<?php session_start(); ?>
<?php include("Database.php");?>
<?php
$databaseOb=new Database();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
				<?php include("socialmanage.php");?>
			</div>
			<div class="container">
				<div class="row menu">
					<?php include("option.php");?>
					<?php include("addToCart.php");?>
				</div>
			</div>
		</section>
			<section class="breadcrums">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 brd-1">
					<h2>Terms & Conditons</h2>
				</div>
			</div>
		
		</div>
		
		</section>
		<section>
			<div class="container">
				<div class="row qanda">
					<h2 class="text-center">
Terms and Conditions | GadgetHouse.com</h2>
					
					<div class="col-md-offset-3 col-md-6 ">
						<div class="questionAns">
						<h4>OVERVIEW</h4>
						<p class="text-justify">This website is operated by Silver Water Technologies Bangladesh Ltd (registered name of GadgetHouse.com). The terms “We”, “Us” and “Our” are used to refer to only GadgetHouse.com or Silver Water Technologies Bangladesh Ltd. GadgetHouse.com offers all information, tools and services which are used or concerns Pickaboo’s carrying out of business, and are publicly available at the website. The user shall be deemed to have accepted all the terms, conditions, policies and notices officially stated, as they continue to use the website and its services.

By visiting our website and/or purchasing something from us, you become a user of the website and engage in our “Services”. Such engagement will consecutively mean the user agreeing to be bound by the following terms and conditions (also referred to as “Terms of Service”, “Terms of use” or “Terms”), including those additional terms & conditions and policies referenced herein and/or available by hyperlink. These Terms of Service apply to all users of the website including, but not limited to, users who are browsers, vendors, customers, merchants, affiliations and/ or contributors of content.

Please read this Terms of Service carefully before accessing or using our website or our services. By accessing or using any part of the website, you agree to be legally bound by these Terms of Service. If you do not agree to all the Terms of this agreement, then you may not access the website or use any of the services.

Any new addition of features, service or tools shall also be deemed to be subject to the Terms of Service in force. You can review the most current version of the Terms of Service at any time on this page. We reserve the right to update, change or replace any part of these Terms & Conditions by posting updates and/or changes to our website. Therefore in order to stay updated on the Terms, please check this page periodically for changes. Your continued use of, or access to the website, following the posting of any changes constitutes acceptance of those changes.</p>
						</div>
						<div class="questionAns">
						<h4>SECTION 1 – GENERAL CONDITIONS</h4>
						<p class="text-justify">By agreeing to these Terms of Service, you represent that you are at the age of majority in your present State or Province of residence, or that you have given us your consent to allow any of your minor dependents to use this website where you are the age of majority in your State or Province of residence.

You may not use our products for any illegal or unauthorized purpose nor may you, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright laws). You must not transmit any worms or viruses or any code of a destructive nature. A breach or violation of any of the Terms will result in an immediate termination of your Services.

We reserve the right to refuse service to anyone for any reason at any time. You understand that your content (not including credit card information), may be transferred unencrypted and may involve the following:

(a) transmissions over various networks; and
(b) changes to conform and adapt to technical requirements of connecting networks or devices.

You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service on the website through which the service is provided, without express written permission by us.</p>
							</div>
						<div class="questionAns">
						<h4>SECTION 2 – AVAILABILITY AND PRICING</h4>
						<p class="text-justify">Availability and pricing of all items are subject to availability. GadgetHouse.com will inform you as soon as possible, if the product(s) and services you have ordered are not available. If not, then the Customer Care will offer similar alternatives.

As part of visiting and/or purchasing anything from Pickaboo, you warrant to agree to understand that due to the nature of the business, availability may change even after an order has been placed. In an event where the product is no longer available, the Customer Care will offer alternatives or the option to cancel your order wholly.

All prices are subject to change without notification, and while every effort is made to ensure the accuracy of the prices displayed on GadgetHouse.com, they are not guaranteed to be accurate. If any price is different from that displayed we will inform you before dispatching the order and you will have the option of continuing with the order or not.

We reserve the right at any time to modify or discontinue any Service (or any part or content thereof) without notice at any time. We shall not be liable to any customer or to any third-party for any modification, price change, suspension or discontinuance of the Service.</p>
							</div>
						<div class="questionAns">
						<h4>SECTION 3 – PRODUCTS</h4>
						<p class="text-justify">Products are available exclusively online through GadgetHouse.com. These products or services may have limited quantities and are subject to return or exchange only according to our Return and Replacement Policy.

Pickaboo has made every reasonable effort to be as accurate as possible in displaying the product in its original color and accurate size. However, due to your device’s display settings, technical issues and differences in display, variations may come to the product’s color or size. Hence, Pickaboo cannot guarantee that the physical product will look exactly like the display image. If a product from Pickaboo is not as described, your sole remedy is to return it unused by fulfilling all the conditions as per our Return and Replacement Policy.

We reserve the right, but are not obligated, to limit the sales of our products or Services to any person, geographic region or jurisdiction. We may exercise this right on a case-by-case basis. We reserve the right to limit the quantities of any products or services that we offer, at any time. All descriptions of products or product pricing are subject to change at any time without notice, at the sole discretion of us. We reserve the right to discontinue any product at any time.</p>
							</div>
						
					</div>
				</div>
			</div>
		</section>
		
		<?php include("websiteEndBody.php");?>
			
			
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>

	

</body>
</html>