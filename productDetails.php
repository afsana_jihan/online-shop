<?php session_start(); ?>
<?php include("Database.php");?>
<?php
$message="";
	$databaseOb=new Database();
	$productName="";
	if (isset($_GET["productId"]) ) {
		$productId = $_GET["productId"];
	}
	
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		$productId=$_POST["productId"];
		if(isset($_POST["addToCart"]) && $_POST["available"]!=0){
			$productId=$_POST["productId"];
			$databaseOb->addToCart($productId,1);
			$message="Product Added to Cart successfully";
		}else if($_POST["available"]==0){
			$message="Sorry, This product is not available";
			$productId = $_POST["productId"];
		}
		
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/magnifier.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
				<?php include("socialmanage.php");?>
			</div>
			<div class="container">
				<div class="row menu">
				
				<?php if(isset(($_SESSION['UserType']))&& $_SESSION['UserType']=='Admin'){
						include("optionAdmin.php");
					}else{
						include("option.php");
						 include("addToCart.php");
					}?>
				</div>
			</div>
		</section>

		<section class="breadcrums">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 brd-1">
					<h2>Product Details</h2>
				</div>
			</div>
		
		</div>
		
		</section>	
	
			<?php $data=$databaseOb->getProductDetails($productId);
			$row = $data->fetch_assoc()?>
			<h4 class="text-center msg2"><?php echo $message;?></h4>	
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-4 productImage">
						<img id="thumb" src="<?php echo $row["ProductImagePath"]?>"class="img-responsive">
					</div>	
					<div class="magnifier-preview col-md-4" id="preview"></div>
							<div class="col-md-4">
							<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
						<div class="productdetails">
							<h3><?php echo $row["ProductName"]?></h3><br>
							<h4>BDT <?php echo $row["ProductPrice"]?></h4><br>
							<h5>Available: <?php echo $databaseOb->AvailableNumberProduct($row["id"])?></h5>
							<div class="col-md-12">
							<p><?php echo $row["ProductDetails"]?></p>
							<input type="hidden"value="<?php echo $row["id"]?>" name="productId">
							<input type="hidden"value="<?php echo $databaseOb->AvailableNumberProduct($row["id"])?>" name="available">
							
							<input class="addcart" type="submit" value="ADD To CART" name="addToCart" >
							
							
							</div>
							
						</div>
						</form>
						</div>
						
						</div>
						</div>
					
					
						
				</div>
			
			
			</div>
			
			<?php include("websiteEndBody.php");?>
			
			
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="assets/js/Event.js"></script>
	<script type="text/javascript" src="assets/js/Magnifier.js"></script>
	<script type="text/javascript">
	var evt = new Event(),
		m = new Magnifier(evt);
		m.attach({
    thumb: '#thumb',
    large: '<?php echo $row["ProductImagePath"]?>',
    largeWrapper: 'preview'
});
	</script>
		<script>$('ul.nav li.dropdown').hover(function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
	}, function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
	});</script>
	
	

</body>
</html>