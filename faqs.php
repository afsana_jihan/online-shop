<?php session_start(); ?>
<?php include("Database.php");?>
<?php
$databaseOb=new Database();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
				<?php include("socialmanage.php");?>
			</div>
			<div class="container">
				<div class="row menu">
					<?php include("option.php");?>
					<?php include("addToCart.php");?>
				</div>
			</div>
		</section>
			<section class="breadcrums">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 brd-1">
					<h2>FAQs</h2>
				</div>
			</div>
		
		</div>
		
		</section>
		<section>
			<div class="container">
				<div class="row qanda">
					<h2 class="text-center">Frequently Asked Questions (FAQs)</h2>
					
					<div class="col-md-offset-3 col-md-6 ">
						<div class="questionAns">
						<h4>1. What is GadgetHouse.com?</h4>
						<p class="text-justify">GadgetHouse.com is an eCommerce site, which started its journey to be the best online shopping platform in Bangladesh. We operate locally to provide service throughout Bangladesh.</p>
						</div>
						<div class="questionAns">
						<h4>2. Why should I shop from GadgetHouse.com?</h4>
						<p class="text-justify">GadgetHouse.com is not just an ordinary online shop. We are proud to stand out from our competitors with our original products, outstanding services, exciting deals and offers. All our products are the best in quality which are supported by warranty as well. We are also offering the most competitive price in the market. Our fastest delivery and after sales service is also a value addition anyone can offer.</p>
							</div>
						<div class="questionAns">
						<h4>3. Are the products genuine?</h4>
						<p class="text-justify">GadgetHouse.com has a wide range of product selections from the most reputed brands and manufacturers worldwide with only genuine products and NO duplicates. You can rest assured that all products showcased on our website are 100% Original! You can also be benefited from our warranty facility.</p>
							</div>
						<div class="questionAns">
						<h4>4. How do I buy from GadgetHouse.com?</h4>
						<p class="text-justify">Our website is designed to make buying online easier, faster, and more reliable for shoppers in Bangladesh. Placing an order online at GadgetHouse.com is very fast and easy. Visit our How to Place an Order Online page for a step-by-step guide on making purchases from GadgetHouse.com</p>
							</div>
						<div class="questionAns">
							
						<h4>5. How do I Pay?</h4>
						<p class="text-justify">We have an extensive range of payment options available for your convenience. You can pay us using bKash, Online Banking, Debit/Credit Cards and obviously Cash!</p>
							</div>
						<div class="questionAns">
						<h4>6. Can I return a product if I am not satisfied?</h4>
						<p class="text-justify">We have a 3 day easy return policy for all products sold online to ensure that our customers are completely satisfied with their purchase. If you are unhappy or unimpressed with your order, we allow returns within 3 days for your satisfaction. Learn more by visiting our Return Policy page</p>
							</div>
						<div class="questionAns">
						<h4>7. Is there a warranty on your products?</h4>
						<p class="text-justify">GadgetHouse.com offers a Brand Warranty, Manufacturer Warranty and Service Warranty on most of the purchased products. Each and every products carry unique warranties which may vary based on the type and brand of product. Learn more by visiting our Warranty Policy page.</p>
					</div>
					</div>
				</div>
			</div>
		</section>
		
		<?php include("websiteEndBody.php");?>
			
			
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>

	

</body>
</html>