<?php session_start(); ?>
<?php include("Database.php");?>
<?php
$databaseOb=new Database();
$loginStatus="";
$email="";
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $obDatabase = new Database();
    if($obDatabase->userLoginCheck($_POST['email'],$_POST['password'])!="False"){
		$_SESSION['IS_LOGGED_IN'] = true;
		$_SESSION['email']=$_POST['email'];
		$_SESSION['UserType']=$obDatabase->userLoginCheck($_POST['email'],$_POST['password']);
		$loginStatus="Login Successful";
		header("Location: index.php");
	}else{
		$loginStatus="Invalid ID/Password!";
		$email=$_POST['email'];
	}
}
if(isset($_SESSION['Msg'])){
	$loginStatus=$_SESSION['Msg'];
	unset($_SESSION['Msg']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
				<?php include("logo.php");?>
				<?php include("head.php");?>
				<?php include("socialmanage.php");?>
			</div>
			<div class="container">
				<div class="row menu">
					<?php include("option.php");?>
					<?php include("addToCart.php");?>
				</div>
			</div>
		</section>
		<section class="breadcrums">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 brd-1">
					<h2>Login</h2>
				</div>
			</div>
		
		</div>
		
		</section>
		<section class="login-page">
			<div class="container">
			<div class="col-md-offset-4 col-md-5">
				<div class="login">
				<h2>Login</h2>
				   <form  method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
						<span>Email:</span></br><input type="text" value="<?php echo $email;?>" name="email"> </br>
						<span>Password:</span></br><input type="password" value="" name="password"></br>
						<span><?php echo $loginStatus;?></span>
						<a href=""><button>Login</button></a>
						<a href="forgetpassword.php"class="forget-pass">forget password?</a>
					</form>
				</div>
		</div>
			</div>
			</div>
		</section>
		<?php include("websiteEndBody.php");?>
		
				
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>


</body>
</html>	
		
		
		
		
		
		