<?php session_start(); ?>
<?php include("Database.php"); ?>
<?php
$success = "";
if ($_SERVER['REQUEST_METHOD'] == 'POST' && !isset($_SESSION['MessageMail'])) {
    $email = $_POST["email"];
	$_SESSION['EmailMail']=$email;
    $obDatabase = new Database();
    if ($obDatabase->userEmailCheck($email) == true) {
        $hash = md5(rand(0, 1000));
		$_SESSION['HashMail']=$hash;
        $obDatabase->updateHashUserTable($hash, $email);
        $success = "Please check your e-mail, we have sent a password reset link to your registered email.";
		header('Location: sendMail.php');  
        
    } else {
        $success = "Invalid Email";
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
		<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
			<?php include("socialmanage.php");?>
			<div class="container">
				<div class="row menu">
					<?php include("option.php");?>
					<?php include("addToCart.php");?>
				</div>
			</div>
			</div>
			<div class="row st_login_form">
		<div class="col-md-offset-3 col-md-6">
		
			<div class="login">
				<h3></h3>
				<?php if(!isset($_SESSION['MessageMail'])){?>
			   <form  method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                    <span>Email</span></br><input type="text" value="" name="email"> </br>
			
					<a><button class="btn-success" type="submit" name="verificationButton">Send Verification Mail</button></a>
                </form>
				<?php }else if(isset($_SESSION['MessageMail'])){?>
					<h3><?php echo $_SESSION['MessageMail']?></h3>
				
				<?php unset($_SESSION['MessageMail']); } ?> 
			</div>
		</div>
</div>

					<?php include("websiteEndBody.php");?>
		














	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>


</body>
</html>