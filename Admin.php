<?php session_start(); ?>
<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
			<?php include("socialmanage.php");?>
			</div>
			</section>
			<?php if(isset(($_SESSION['UserType']))&& $_SESSION['UserType']=='Admin'){?>
			<div class="row options">
				<?php include("admin_options.php");?>
			</div>

			<div class="container adminMain">
					<div class="col-md-12">
						<h2 class="text-center">Hello Admin</h2>
						
						
						
					</div>
				</div>
	<?php }else{
		echo '<h2  class="text-center">Access Denied!!!</h2>';
	}?>
			<?php include("websiteEndBody.php");?>



	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>


</body>
</html>
			