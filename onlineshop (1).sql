-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2018 at 08:23 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onlineshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `carttable`
--

CREATE TABLE `carttable` (
  `OrderID` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orderanddeliverytable`
--

CREATE TABLE `orderanddeliverytable` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `CustomerName` varchar(1000) NOT NULL,
  `CustomerAddress` varchar(1000) NOT NULL,
  `CustomerNumber` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderanddeliverytable`
--

INSERT INTO `orderanddeliverytable` (`id`, `productId`, `quantity`, `status`, `CustomerName`, `CustomerAddress`, `CustomerNumber`) VALUES
(2, 17, 1, 1, 'Afsana', 'Didar Market', '01820948751'),
(3, 23, 1, 1, 'Faria Sultana', 'chawkbazar', '018192283848'),
(4, 25, 3, 1, 'Faria Sultana', 'chawkbazar', '018192283848');

-- --------------------------------------------------------

--
-- Table structure for table `productstocktable`
--

CREATE TABLE `productstocktable` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productStocked` int(11) NOT NULL,
  `productSold` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productstocktable`
--

INSERT INTO `productstocktable` (`id`, `productId`, `productStocked`, `productSold`) VALUES
(2, 17, 78, 0),
(3, 23, 1, 0),
(4, 25, 0, 0),
(5, 24, 5, 0),
(6, 0, 5, 0),
(7, 28, 4, 0),
(8, 29, 5, 0),
(9, 31, 54, 0);

-- --------------------------------------------------------

--
-- Table structure for table `producttable`
--

CREATE TABLE `producttable` (
  `id` int(11) NOT NULL,
  `ProductImageName` varchar(100) NOT NULL,
  `ProductImagePath` varchar(100) NOT NULL,
  `ProductCode` varchar(100) NOT NULL,
  `ProductName` varchar(100) NOT NULL,
  `ProductPrice` double NOT NULL,
  `ProductDetails` varchar(1000) NOT NULL,
  `ProductCategory` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `producttable`
--

INSERT INTO `producttable` (`id`, `ProductImageName`, `ProductImagePath`, `ProductCode`, `ProductName`, `ProductPrice`, `ProductDetails`, `ProductCategory`) VALUES
(17, 'Mobile_01.jpg', 'productImage/Mobile_01.jpg', 'Mobile-01', 'Samsung Galaxy A5- 2016', 20000, 'Samsung Galaxy A5 2016 Android v5.1.1 Lollipop 5.2-inch Super AMOLED Multi-Touchscreen Protective Co', 'Mobile'),
(19, 'Mobile_03.jpg', 'productImage/Mobile_03.jpg', 'Mobile-03', 'Iphone 8 plus', 105000, 'iPhone 8 Plus Smartphone 64GB | Apple\r\nOS: iOS 11\r\nSIM Card: Nano-SIM\r\nProcessor: Embedded M11 motio', 'Mobile'),
(21, 'Mobile_05.jpg', 'productImage/Mobile_05.jpg', 'Mobile-05', 'LG Stylus 3', 18900, 'LG Stylus 3 RAM: 3GB; ROM:16GB\r\nProcessor: Octa Core 1.5GHz\r\nDisplay: 5.7 inches (720 x 1280) HD In ', 'Mobile'),
(23, 'Tablet_01.jpg', 'productImage/Tablet_01.jpg', 'Tablet-01', 'Samsung Tab J Max', 16900, 'Samsung Tab J Max 1.5GB/8GB\r\nOS: Android 5.1 (Lollipop)\r\nProcessor: Quad-core 1.5 GHz Cortex-A7\r\nChi', 'Tablet'),
(24, 'Mobile_07.jpg', 'productImage/Mobile_07.jpg', 'Mobile-07', 'Nokia 6 with', 22500, 'Nokia 6 Smartphone: Display\r\nThe Nokia 6 smartphone comes with a 5.50-inch touchscreen display with ', 'Mobile'),
(25, 'Laptop_04.jpg', 'productImage/Laptop_04.jpg', 'Laptop-04', 'Acer Aspire E5-474 6th', 35090, 'Acer Aspire E5-474 6th Gen Intel Core i3 14 Inch\r\nProcessor: IntelÂ® Coreâ„¢ i3-6100U Processor (3M ', 'Laptop'),
(26, 'Accessories_02.jpg', 'productImage/Accessories_02.jpg', 'Accessories-02', 'ADATA 1TB HV620S ', 4090, 'The ultra-portable HV620S packs up to 1TB of external storage in a drive thatâ€™s only 11.5mm thick.', 'Accessories'),
(27, 'Tablet_03.jpg', 'productImage/Tablet_03.jpg', 'Tablet-03', 'Huawei MediaPad T3 7 1GB/8GB', 10900, 'Huawei MediaPad T3 7 1GB/8GB with Free Flip Cover\r\nOS: Android 7.0 (EMUI 5.1)\r\nDisplay: 7â€IPS (600', 'Tablet'),
(28, 'Accessories_12.jpg', 'productImage/Accessories_12.jpg', 'Accessories-12', 'cooling pad', 1200, 'Black Cat Laptop Cooling Pad with 3 Quiet Fans-(14\'-17\')\r\n14\'-17\' Laptop Cooling Pad with Triple 110', 'Accessories'),
(31, 'Laptop_45.jpg', 'productImage/Laptop_45.jpg', 'Laptop-45', 'ACER', 30000, 'Acer Aspire E5-474 6th Gen Intel Core i3 14 Inch\r\nProcessor: IntelÂ® Coreâ„¢ i3-6100U Processor (3M Cache, 2.30 GHz)\r\nRAM: 4GB DDR3L\r\nROM: DVD+RW\r\nHard Drive: 1TB\r\nDisplay: 14\" HD widescreen CineCrystal display (1366 x 768)\r\nGraphics: IntelÂ® HD Graphics 520 \r\nBattery: 4 cell battery 5 hours', 'Laptop');

-- --------------------------------------------------------

--
-- Table structure for table `usercommenttable`
--

CREATE TABLE `usercommenttable` (
  `id` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Comment` varchar(500) NOT NULL,
  `IsReply` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usercommenttable`
--

INSERT INTO `usercommenttable` (`id`, `Name`, `Email`, `Comment`, `IsReply`) VALUES
(1, 'AAA', 'sacsac', 'xc', 0),
(2, '', '', '', 0),
(3, 'Afsana akther chowdhury', 'afsanajihan45@gmail.com', 'Ki sob product den vai\r\nJottosob Faltu Service', 0),
(4, 'Farhana Jhimu', 'jhimu@gmail.com', 'Josh josh', 0);

-- --------------------------------------------------------

--
-- Table structure for table `usertable`
--

CREATE TABLE `usertable` (
  `id` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Number` varchar(14) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `forgetPasswordIdentity` varchar(1000) NOT NULL,
  `UserType` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usertable`
--

INSERT INTO `usertable` (`id`, `Name`, `Email`, `Number`, `Password`, `forgetPasswordIdentity`, `UserType`) VALUES
(1, 'afsana akther chowdhury', 'afsanajihan45@gmail.com', '01839231133', 'd3620552c8df5c53c235860f4c1e29cc', '42998cf32d552343bc8e460416382dca', 'Customer'),
(2, 'Faria Sultana', 'faria@gmail.com', '018293300940', '5d41402abc4b2a76b9719d911017c592', '', 'Customer'),
(3, 'Safrana Tabassum Bushra', 'safranabushra11@gmail.com', '903949500606', '81dc9bdb52d04dc20036dbd8313ed055', '', 'Customer'),
(4, 'My Name Admin', 'admin@gmail.com', '01834567784', '21232f297a57a5a743894a0e4a801fc3', '', 'Admin'),
(5, 'ascas', 'xz', '5645', '0f7afb32af84c2abd56f15da94158c02', '', 'Customer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carttable`
--
ALTER TABLE `carttable`
  ADD PRIMARY KEY (`OrderID`),
  ADD KEY `ProductId` (`ProductId`);

--
-- Indexes for table `orderanddeliverytable`
--
ALTER TABLE `orderanddeliverytable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productstocktable`
--
ALTER TABLE `productstocktable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `producttable`
--
ALTER TABLE `producttable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usercommenttable`
--
ALTER TABLE `usercommenttable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usertable`
--
ALTER TABLE `usertable`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carttable`
--
ALTER TABLE `carttable`
  MODIFY `OrderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `orderanddeliverytable`
--
ALTER TABLE `orderanddeliverytable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `productstocktable`
--
ALTER TABLE `productstocktable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `producttable`
--
ALTER TABLE `producttable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `usercommenttable`
--
ALTER TABLE `usercommenttable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `usertable`
--
ALTER TABLE `usertable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carttable`
--
ALTER TABLE `carttable`
  ADD CONSTRAINT `carttable_ibfk_1` FOREIGN KEY (`ProductId`) REFERENCES `producttable` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
