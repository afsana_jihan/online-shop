<?php session_start(); ?>
<?php include("Database.php");?>
<?php
$databaseOb=new Database();
$message="";
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		if(isset($_POST["addToCart"]) && $_POST["available"]!=0){
			$productId=$_POST["productId"];
			$databaseOb->addToCart($productId,1);
			$message="Product Added to Cart successfully";
		}
		else{
			
			$message="Sorry, This product is not available";
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
				<?php include("socialmanage.php");?>
			</div>
			<div class="container">
				<div class="row menu">
					<?php if(isset(($_SESSION['UserType']))&& $_SESSION['UserType']=='Admin'){
						include("optionAdmin.php");
					}else{
						include("option.php");
						 include("addToCart.php");
					}?>
					
					
				</div>
			</div>
		</section>
			<section class="slider">
				<div class="container-fluid">
					<div class="row">
						

         
	<section id="lab_video_slider">
		<div class="container-fluid">
			<div class="row">				
				<div class="swiper-container">
					<div class="swiper-wrapper">						
						<div id="slide_two" class="swiper-slide">
							<div id="lab_video_text_overlay">
								<div class="container">
									<h4>GadgetHouse.com </h4>
									<p>A trusted online-shop for your desired products in Bangladesh.</p> 	
									<div class="search">
									<form method="POST" action="searchpage.php">
										  <span class="fa fa-search"></span>
										  <input name="search" value="" placeholder="Search products">
										  <a href=""><button name="submit" value="submit" class="searchProductbtn"> Search</button></a>
									</form>
									</div>	 
</div>
							</div>
							<video class="slider-video" width="100%" preload="auto" loop="" autoplay="" style="visibility: visible; width: 100%;" poster="">
								<source src="assets/img/1.mp4" type="video/mp4">
							</video>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
  	
					</div>
				
				</div>
			
			</section>		
			<section class="special-offer">
				<div class="container">
					<div class="row">
					<h2 class="text-center">Products</h2>
					<h4 class="text-center msg"><?php echo $message;?></h4>	
					<?php $allProduct=$databaseOb->getAllProduct("");
					$p=0;
					while ($row = $allProduct->fetch_assoc()) {
						$p++;
						if($p==9)break;?>
						
						<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
						<div class="col-md-offset-1 col-md-2 offer-content">
							<a href="productDetails.php?productId=<?php echo $row["id"]?>"><img src="<?php echo $row["ProductImagePath"]?>" class="img-responsive"></a>
							<h6><?php echo $row["ProductName"]?></h6>
							<p>BDT <label><?php echo $row["ProductPrice"]?></label></p>
							<p>Available: <label><?php echo $databaseOb->AvailableNumberProduct($row["id"])?></label></p>
							<input type="hidden"value="<?php echo $row["id"]?>" name="productId">
							<input type="hidden"value="<?php echo $databaseOb->AvailableNumberProduct($row["id"])?>" name="available">
							<input class="add-to-cart" type="submit" value="ADD To CART" name="addToCart">
						</div>
						</form>
					<?php } ?>	
				
					</div>
				</div>
			</section>
		
			
			<?php include("websiteEndBody.php");?>
			
			
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>

	<script>
	// without this script, the slider doesn't start on it's own:
  !function ($) {
    $(function(){
      $('#homepage_slider').carousel()
    })
  }(window.jQuery)


// if user chooses to not autoplay the video, the button should be uncommented in html and this script will make the button work:
var vid = document.getElementById("bgvid");
var playButton = document.querySelector("#slider-play-button button");

playButton.addEventListener("click", function() {
  if (vid.paused) {
    vid.play();
	playButton.classList.remove("play-video-button");
    playButton.classList.add("pause-video-button");
  } else {
    vid.pause();
	playButton.classList.add("play-video-button");
    playButton.classList.remove("pause-video-button");
  }
});</script>
<script>$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});</script>
</body>
</html>