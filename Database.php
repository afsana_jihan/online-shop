<?php
class Database{
	private $servername="localhost";
	private $username="root";
	private $password="";
	private $dbname="onlineshop";
	public $conn;
	
	function __construct() {
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
    }

    function createTable($sqlCommand) {
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $conn->query($sqlCommand);
        if ($conn->connect_error) {
            echo $conn->connect_error;
        }
    }
	function insertNewUserToUserTable($name,$email,$phone,$password){
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
		$password=md5($password);
        $sqlCommand = "INSERT INTO UserTable( Name, Email, Number, Password,forgetPasswordIdentity,UserType) "
                . "VALUES ('$name','$email','$phone','$password','','Customer')";
        $conn->query($sqlCommand);
        if ($conn->connect_error) {
            echo $conn->connect_error;
        }
	}
	function insertNewCommentToUserCommentTable($name,$email,$comment){
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $sqlCommand = "INSERT INTO UserCommentTable( Name, Email,Comment) "
                . "VALUES ('$name','$email','$comment')";
        $conn->query($sqlCommand);
        if ($conn->connect_error) {
            echo $conn->connect_error;
        }
	}
	function insertNewProductIntoOrderTable($productId,$quantity,$name,$address,$number){
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $sqlCommand = "INSERT INTO orderanddeliverytable( productId, quantity,status,CustomerName,CustomerAddress,CustomerNumber) "
                . "VALUES ('$productId','$quantity',0,'$name','$address','$number')";
        $conn->query($sqlCommand);
        if ($conn->connect_error) {
            echo $conn->connect_error;
        }
	}
	function getAllDataFromCart()
	{
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $query = "SELECT producttable.id,producttable.ProductName, producttable.ProductPrice,carttable.quantity,(carttable.quantity*producttable.ProductPrice) as totalprice
		FROM carttable Inner JOIN producttable ON producttable.id=carttable.ProductId";
        $result = $conn->query($query);
        return $result;
	}
	function getAllDataFromOrderAndDeliveryTable($status)
	{
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $query = "SELECT orderanddeliverytable.id,producttable.ProductCode,producttable.ProductName, producttable.ProductPrice,orderanddeliverytable.quantity,(orderanddeliverytable.quantity*producttable.ProductPrice) as totalprice,orderanddeliverytable.CustomerName,orderanddeliverytable.CustomerAddress,orderanddeliverytable.CustomerNumber FROM orderanddeliverytable Inner JOIN producttable ON producttable.id=orderanddeliverytable.ProductId where orderanddeliverytable.status='$status' order by CustomerName";
        $result = $conn->query($query);
        return $result;
	}
	function updateStatusInorderanddeliverytable($orderPId){
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
		$sqlCommand ="update orderanddeliverytable set status='1' WHERE id= '$orderPId'";
        $conn->query($sqlCommand);
	}
	function updateInProductCart($productId,$updateValue){
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
		$sqlCommand ="update carttable set quantity='$updateValue' WHERE productId= '$productId'";
        $conn->query($sqlCommand);
	}
	function RemoveFromProductCart($productId){
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
		$sqlCommand ="Delete FROM carttable WHERE productId= '$productId' ";
        $conn->query($sqlCommand);
	}
	function totalProductCart()
	{
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
		$query = "SELECT * FROM carttable";
        $result = $conn->query($query);
        return $result->num_rows;
	}
	function addToCart($productId,$quantity){
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
		$query = "SELECT * FROM carttable WHERE productId= '$productId' ";
        $result = $conn->query($query);
        if ($result->num_rows == 1) {
			$sqlCommand ="update carttable set quantity=quantity+1 WHERE productId= '$productId'";
		}
		else{
			$sqlCommand = "INSERT INTO carttable(ProductId, quantity) VALUES ('$productId','$quantity')";
		}
        $conn->query($sqlCommand);
        if ($conn->connect_error) {
            echo $conn->connect_error;
        }
	}
	function userLoginCheck($email, $password) {
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
		$password=md5($password);
        $query = "SELECT * FROM UserTable WHERE Email= '$email' && Password = '$password'";
        $result = $conn->query($query);
        if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            return $row["UserType"];
        } else {
            return "False";
        }
    }
	
	function userEmailCheck($email) {
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $query = "SELECT * FROM UserTable WHERE email = '$email'";
        $result = $conn->query($query);
        if ($result->num_rows == 1) {
            return true;
        } else {
            return false;
        }
    }
	function updateHashUserTable($hash, $email) {
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $query = "update UserTable set forgetPasswordIdentity='$hash' WHERE email= '$email'";
        $conn->query($query);
    }
	
	function updatePasswordInUserTable($password, $email) {
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
		$password=md5($password);
        $query = "update UserTable set password='$password' WHERE Email= '$email'";
        $conn->query($query);
    }
	function userHashAndEmailCheck($email, $hash) {
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $query = "SELECT * FROM UserTable WHERE Email= '$email' && forgetPasswordIdentity= '$hash'";
        $result = $conn->query($query);
        if ($result->num_rows == 1) {
            return true;
        } else {
            return false;
        }
    }
	
	function getNameFromUserTable($email) {
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $query = "SELECT name FROM UserTable WHERE email= '$email' ";
        $result = $conn->query($query);
        if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            return $row["name"];
        }
    }
	function getAllProduct($productName){
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $query = "SELECT * FROM productTable ";
		if($productName!=""){
			$query= $query."where ProductCategory='$productName'";
		}
        $result = $conn->query($query);
        return $result;
	}
	function getAllSearchProduct($searchString){
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $query = "SELECT * FROM productTable WHERE ProductName like '%$searchString%' OR ProductCode like '%$searchString%'";
        $result = $conn->query($query);
        return $result;
	}
	function getAllFromCart(){
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $query = "SELECT * FROM carttable";
        $result = $conn->query($query);
        return $result;
	}
	
	function getProductDetails($productId){
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $query = "SELECT * FROM productTable where id='$productId'";
        $result = $conn->query($query);
        return $result;
	}
	
		function StockInToProductStockTable($productId,$stockInNumber){
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
		$query = "SELECT * FROM productStockTable WHERE productId= '$productId'";
        $result = $conn->query($query);
        if ($result->num_rows == 1) {
            $query="UPDATE productStockTable SET productStocked = productStocked + '$stockInNumber' WHERE productId ='$productId'";
			$conn->query($query);
        }else{
        $sqlCommand = "INSERT INTO productStockTable(productId,productStocked) "
                . "VALUES ('$productId','$stockInNumber')";
				$conn->query($sqlCommand);
		}
        
        if ($conn->connect_error) {
            echo $conn->connect_error;
        }
	}
	function ClearCartTable()
	{
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $query = "DELETE FROM carttable";
        $result = $conn->query($query);
        return $result;
	}
	function GetAllAvailableProduct()
	{
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $query = "SELECT producttable.id,producttable.ProductCode,producttable.ProductName,producttable.ProductPrice,IFNULL(productStockTable.productStocked, 0) as productStocked,IFNULL(productStockTable.productSold, 0) as productSold FROM producttable LEFT JOIN productStockTable ON producttable.id=productStockTable.productId order by productCode";
        $result = $conn->query($query);
		return $result;
	}
	function AvailableNumberProduct($productId)
	{
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $query = "SELECT productStocked FROM productStockTable WHERE productId='$productId'";
        $result = $conn->query($query);
		if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            return $row["productStocked"];
        }else{
			return 0;
		}
	}
	function RemoveProduct($productId){
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $query = "DELETE FROM producttable WHERE id='$productId'";
        $result = $conn->query($query);
		$query = "DELETE FROM productStockTable WHERE productId='$productId'";
        $result = $conn->query($query);
		
	}
}
?>