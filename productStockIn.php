<?php session_start(); ?>
<?php include("Database.php");?>
<?php
	$databaseOb=new Database();
	$productName="";
	if (isset($_GET["product"]) ) {
		$productName = $_GET["product"];
	}
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["productId"]) && isset($_POST["stockedInProduct"] )){
		$databaseOb->StockInToProductStockTable($_POST["productId"],$_POST["stockedInProduct"]);
		?>
		<script>
			alert("Successfully Added");
		</script>
		<?php
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
			<?php include("socialmanage.php");?>
			</div>
			</section>
			<?php if(isset(($_SESSION['UserType']))&& $_SESSION['UserType']=='Admin'){?>
			
			<div class="row options">
				<?php include("admin_options.php");?>
			</div>

			<div class="container">
					<div class="col-md-offset-3 col-md-6  adminMain">
						<h2 class="text-center">Hello Admin</h2>
						
						<h3 class="text-center">Product Stock-In</h3>
						
						<form action="?product=<?php echo $productName?>" method="POST">
						<div class="addproductoption">
						<h5>Product Category</h5>
						<ul>
							<li><a href="?product=Laptop"><i class="fa fa-angle-right"></i> Laptop</a></li>
							<li><a href="?product=Mobile"><i class="fa fa-angle-right"></i> Mobile</a></li>
							<li><a href="?product=Tablet"><i class="fa fa-angle-right"></i> Tablet</a></li>
							<li><a href="?product=Accessories"><i class="fa fa-angle-right"></i>  Accessories</a></li>
						</ul>
						</div>
						<div class="addproductoption">
						<h5>Product Name</h5>
						<select name="productId" id="productId">
							<option value="">---Choose Anything---</option>
							<?php $allProduct=$databaseOb->getAllProduct($productName);
							while ($row = $allProduct->fetch_assoc()) {?>
							<option value="<?php echo $row["id"]?>"><?php echo $row["ProductName"]?></option>
							<?php } ?>
						</select></div>
						<div class="addproductoption">
						<h5>Number Of Product you want to stocked</h5>
						<input type="number" name="stockedInProduct" id="stockedInProduct">
						</div>
						<a><button class="btn btn-success img_submit">Submit</button></a>
						</form>
						
					</div>
				</div>
			<?php }else{
		echo '<h2  class="text-center">Access Denied!!!</h2>';
	}?>
			<?php include("websiteEndBody.php");?>



	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>


</body>
</html>
			