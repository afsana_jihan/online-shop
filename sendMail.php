<?php session_start(); ?>
<?php include("Database.php"); ?>
<?php
$obDatabase = new Database();
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer-master/src/Exception.php';
require 'PHPMailer-master/src/PHPMailer.php';
require 'PHPMailer-master/src/SMTP.php';
//Load composer's autoloader

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'gadgethousehome@gmail.com';                 // SMTP username
    $mail->Password = 'gadgethouse112277';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to
	
	$resetPassLink = 'http://localhost/online%20shop/resetpassword.php?fp_code=' . urldecode($_SESSION['HashMail']) . '&email=' . urldecode($_SESSION['EmailMail']);
    
	//Recipients
    $mail->setFrom('gadgethousehome@gmail.com', 'Gadget House Online Shop');
    $mail->addAddress($_SESSION['EmailMail'], '');     // Add a recipient
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Password Update Request';
    $mail->Body    = 'Dear ' . $obDatabase->getNameFromUserTable($_SESSION['EmailMail']) . ', 
                Recently a request was submitted to reset a password for your account. If this was a mistake, just ignore this email and nothing will happen.
                To reset your password, visit the following link:  href="' . $resetPassLink . '"<br>
                Regards,
                Gadget House ';
    //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
	$_SESSION['MessageMail']='Message has been sent';
	header('Location: forgetpassword.php'); 
	
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}