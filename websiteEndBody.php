			<section class="summary pt30">
			<div class="container">
				<div class="row">
					<div class="col-md-3 about-us">
						<h3>About us</h3>
						<p class="text-justify">GadgetHouse.com is an ultimate shopping destination where you can shop the Smart and Feature phones,Computing and accessories, have them delivered to you directly. We offer Cash on delivery and bKash with affordable price and quality products.Happy Shopping!</p>
					</div>
					<div class="col-md-2 menu-last">
						<h3>Menu</h3>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li><a href="products.php">Products</a></li>							
						</ul>
					</div>
					<div class="col-md-2 service">
						<h3>Customer Services</h3>
						<ul>
							<li><a href="termsCondition.php">Terms & Conditions</a></li>
							<li><a href="faqs.php">FAQs</a></li>
							<li><a href="contact.php">Contact Us</a></li>
						</ul>
					</div>
					<div class="col-md-2 my-account">
						<h3>My Account</h3>
						<ul>
							<li><a href="login.php">Login</a></li>
							<li><a href="register.php">Register</a></li>
							
					</div>
				</div>
			</div>
			</section>
			<section class="top-footer">
			<div class="container-fluid">
				<div class="row logo-last">
					<div class="col-md-offset-3 col-md-6">
						<img src="assets/img/logo.png"> 
						<p>A trusted online-shop for your desired products in Bangladesh.</p>
					</div>
				</div>
				<div class="row  social social-last">
				<div class="col-md-offset-5 col-md-5">
					<ul>
						<li><a href="https://www.facebook.com"><i class="fa fa-facebook"></i><span></span></a></li>
						<li><a href="https://www.twitter.com"><i class="fa fa-twitter"></i><span></span></a></li>
						<li><a href="https://www.pinterest.com"><i class="fa fa-pinterest"></i><span></span></a></li>
						<li><a href="https://www.googleplus.com"><i class="fa fa-google-plus"></i><span></span></a></li>
					</ul>
				</div>
			</div>
			<div class="row last-footer">
				<div class=" col-md-3">
					<p><i class="fa fa-home"></i> 99,lalkhan Bazar,ctg.</p>
				</div>
					<div class="col-md-offset-1 col-md-3">
					<p><i class="fa fa-phone"></i> +1234 758 839 , +1273 748 730</p>
				</div>
					<div class="col-md-offset-1 col-md-3">
					<p class="email"><i class="fa fa-envelope"></i><a href="https://www.gmail.com" > gadgethousehome@gmail.com</a></p>
				</div>
				
			</div>
			</div>
			
			</section>
			<footer>
				<p>&copy; 2018 Gadget House.All Rights Reserved</p>
			
			</footer>