<?php session_start(); ?>
<?php include("Database.php");?>
<?php
$databaseOb=new Database();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
		if(isset($_POST["update"])){
			$databaseOb->updateInProductCart($_POST["productId"],$_POST["quantity"]);
		}
		if(isset($_POST["remove"])){
			$databaseOb->RemoveFromProductCart($_POST["productId"]);
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
				<?php include("socialmanage.php");?>
			</div>
			<div class="container">
				<div class="row menu">
					<?php if(isset(($_SESSION['UserType']))&& $_SESSION['UserType']=='Admin'){
						include("optionAdmin.php");
					}else{
						include("option.php");
					}?>
					
					<?php include("addToCart.php");?>
				</div>
			</div>
		</section>
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8 modal-body">
					<table>
					
												<thead>
														<tr>
														<th>Sr</th>
														<th>Product Name</th>
														<th colspan="2">Quantity</th>
														<th>Unit Price</th>
														<th>Price</th>
														</tr>
													</thead>
													<tbody>
													<?php $i=1; $sum=0;
													$allCartData=$databaseOb->getAllDataFromCart();
													while ($row = $allCartData->fetch_assoc()) {?>
													<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
														<tr>
														<td><?php echo $i++;?></td>
														<td><?php echo $row["ProductName"];?> </td>
														<td><input type="number" value="<?php echo $row["quantity"]?>"name="quantity"></td>
														<td>
														<input class="btn btn-warning" type="submit" value="update" name="update"></td>
						
														<td><?php echo $row["ProductPrice"];?> Tk</td>
														<td><?php $sum+=$row["totalprice"];echo $row["totalprice"];?> Tk</td>
														<td>
														<input class="btn btn-warning" type="submit" value="Remove" name="remove"></td>
														</tr>
														<input type="hidden"value="<?php echo $row["id"]?>" name="productId">
							
													</form>
													<?php }?>
												</tbody>
												<tfoot>
														<tr>
														  <td>TOTAL</td>
														  <td></td>
														  <td></td>
														  <td></td>
														  <td>BDT</td>
														  <td><?php echo $sum;?> Tk</td>
														</tr>
													</tfoot>
											</table>
											<a href="cardpayment.php"><button type="button" class="btn btn-success purchase" >PURCHASE</button></a>
				</div>
			</div>
		</div>
		
				<?php include("websiteEndBody.php");?>
		
				
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>


</body>
</html>	