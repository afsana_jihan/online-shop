<?php session_start(); ?>
<?php include("Database.php");?>
<?php
$databaseOb=new Database();
if(isset($_POST['name']) && isset($_POST['deliAddress'])&& isset($_POST['phone']))
{	

$allProduct=$databaseOb->getAllFromCart();
while ($row = $allProduct->fetch_assoc()) {
	$productId=$row['ProductId'];
	$stockOut=$row['quantity'];
	$databaseOb->insertNewProductIntoOrderTable($productId,$stockOut,$_POST['name'],$_POST['deliAddress'],$_POST['phone']);
	$stockOut=-1*$stockOut;
	$databaseOb->StockInToProductStockTable($productId,$stockOut);
}

$databaseOb->ClearCartTable();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
				<?php include("socialmanage.php");?>
			</div>
			<div class="container">
				<div class="row menu">
					<?php if(isset(($_SESSION['UserType']))&& $_SESSION['UserType']=='Admin'){
						include("optionAdmin.php");
					}else{
						include("option.php");
					}?>
					
					<?php include("addToCart.php");?>
				</div>
			</div>
		</section>
		<?php if(isset($_SESSION['IS_LOGGED_IN'])&& $_SESSION['IS_LOGGED_IN']==true){?>
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8 modal-body">
					<div style="margin-left: 10%;">
						<h3>Purchase Successful<h3>
						<h5>You will get your product with in 24 hours,InshaAllah.</h5>
						<h5>Thank You for choosing GadgetHouse.com</h5>
					</div>
				</div>
			</div>
		</div>
		<?php }else{?>
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8 modal-body">
					<div style="margin-left: 10%;">
						<h3>Access Denied!!!<h3>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		
		<?php include("websiteEndBody.php");?>
		
				
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>


</body>
</html>	