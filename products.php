<?php session_start(); ?>
<?php include("Database.php");?>
<?php
	$message="";
	$databaseOb=new Database();
	$productName="";
	if (isset($_GET["product"]) ) {
		$productName = $_GET["product"];
	}
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		if(isset($_POST["addToCart"]) && $_POST["available"]!=0){
			$productId=$_POST["productId"];
			$databaseOb->addToCart($productId,1);
			$message="Product Added to Cart successfully";
		}
		else{
			
			$message="Sorry, This product is not available";
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
			<?php include("socialmanage.php");?>
			</div>
			<div class="container">
				<div class="row menu">
					<?php if(isset(($_SESSION['UserType']))&& $_SESSION['UserType']=='Admin'){
						include("optionAdmin.php");
					}else{
						include("optionForProduct.php");
						 include("addToCart.php");
					}?>
				</div>
			</div>
		</section>
		<section class="breadcrums">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 brd-1">
					<h2>Products</h2>
				</div>
			</div>
		
		</div>
		
		</section>
		<div class="container sm120 slide">
		<div class="row">
			<div class="col-sm-12">
				<div id="my-slider" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#my-slider" data-slide-to="0" class="active"></li>
						<li data-target="#my-slider" data-slide-to="1"></li>
						<li data-target="#my-slider" data-slide-to="2"></li>
					</ol>
				
				
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<img src="assets/img/1.jpg">
					
						</div>
						<div class="item">
							<img src="assets/img/2.jpg">
							
						</div>
						<div class="item">
							<img src="assets/img/3.jpg">
						
						</div>
					
					</div>
					<a class="left carousel-control " href="#my-slider" role="button" data-slide="prev">
					<span><i class="fa fa-chevron-left sm400" aria-hidden="true"></i></span>
					</a>
					
					<a class="right carousel-control" href="#my-slider" role="button" data-slide="next">
						<span><i class="fa fa-chevron-right  sm400 " aria-hidden="true"></i></span>
					</a>
				</div>
			
			</div>
		</div>
	</div>
		<section class="special-offer">
				<div class="container">
					<h2>Products</h2>
					<div class="row">
					
					<?php $allProduct=$databaseOb->getAllProduct($productName);?>
					<h4 class="text-center msg"><?php echo $message;?></h4>	<?php
					while ($row = $allProduct->fetch_assoc()) {?>
						<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
						<div class="col-md-offset-1 col-md-2 offer-content">
							<a href="productDetails.php?productId=<?php echo $row["id"]?>"><img src="<?php echo $row["ProductImagePath"]?>" class="img-responsive text-center"></a>
							<h6><?php echo $row["ProductName"]?></h6>
							<p>BDT <label><?php echo $row["ProductPrice"]?></label></p>
							<p>Available: <label><?php echo $databaseOb->AvailableNumberProduct($row["id"])?></label></p>
							<input type="hidden"value="<?php echo $row["id"]?>" name="productId">
							<input type="hidden"value="<?php echo $databaseOb->AvailableNumberProduct($row["id"])?>" name="available">
							<input class="add-to-cart" type="submit" value="ADD To CART" name="addToCart">
						</div>
						</form>
					<?php } ?>	
					</div>
				</div>
			</section>
			<?php include("websiteEndBody.php");?>






	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
	</script>
	<script>$('ul.nav li.dropdown').hover(function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
	}, function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
	});</script>

</body>
</html>