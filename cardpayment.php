<?php session_start(); ?>
<?php include("Database.php");?>
<?php
$databaseOb=new Database();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
				<?php include("socialmanage.php");?>
			</div>
			<div class="container">
				<div class="row menu">
					<?php if(isset(($_SESSION['UserType']))&& $_SESSION['UserType']=='Admin'){
						include("optionAdmin.php");
					}else{
						include("option.php");
					}?>
					
					<?php include("addToCart.php");?>
				</div>
			</div>
		</section>
		<?php if(isset($_SESSION['IS_LOGGED_IN'])&& $_SESSION['IS_LOGGED_IN']==true){?>
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8 modal-body">
					<div style="margin-left: 10%;">
						<h3>Payment Information</h3>
						<h5>Please enter your credit card information below & click submit.</h5>
					</div>
		<form method="POST" action="thanks.php" onsubmit="return !!(phonenumber(this) && validateCardNumber(this) && cardname(this) && delname(this))">
        <label>Credit card accepted</label>
        <div align="center" style="font-size: 35px;">
            <input type="radio" name="card" value="visa" required><i class="fa fa-cc-visa"></i>
            <input type="radio" name="card" value="master" required><i class="fa fa-cc-mastercard"></i>
            <input type="radio" name="card" value="amex" required><i class="fa fa-cc-amex"></i>
            <input type="radio" name="card" value="discover" required><i class="fa fa-cc-discover"></i>
        </div>

        <label>Card Number</label>
        <div align="center">
            <input class="form-control" type="number" name="cardNumber" required><br>
        </div>

       
	   

      

        <label>Name</label>
        <div align="center">
            <input class="form-control" type="text" name="name" required><br>
        </div>

        <label>E-mail</label>
        <div align="center">
            <input class="form-control" type="email" name="email" required><br>
        </div>

        <label>Delivery Address</label>
        <div align="center">
            <textarea name="deliAddress" class="form-control" id="" cols="50" rows="3" placeholder="Address please.." required></textarea>
        </div>


        <label>Phone Number</label>
        <div align="center">
            <input class="form-control" type="number" name="phone" required><br>
        </div>
        <input type="submit" name="submit" class="btn btn-success">

    </form>
				</div>
			</div>
		</div>
		<?php }else{ ?>
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8 modal-body">
					<div style="margin-left: 10%;">
						<h3>Please Login<h3>
					</div>
				</div>
			</div>
		</div>
		
		<?php }?>
		<?php include("websiteEndBody.php");?>
		
				
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>


</body>
</html>	