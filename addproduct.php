<?php session_start(); ?>
<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
			<?php include("socialmanage.php");?>
			</div>
			</section>
			<?php if(isset(($_SESSION['UserType']))&& $_SESSION['UserType']=='Admin'){?>
			
			<div class="row options">
				<?php include("admin_options.php");?>
			</div>

			<div class="container">
					<div class="col-md-offset-3 col-md-6 adminMain">
						<h2 class="text-center">Hello Admin</h2>
						<h3 class="text-center">Add Product</h3>
					<form action="upload.php" method="post" enctype="multipart/form-data">
					<div class="addproductoption">
						<h4>1.Upload your Products Picture here</h4>
						<input type="file" name="image">
					</div>
					<div class="addproductoption">
						<h4>2.Add the Details of Product here</h4>
						<h5>a)Product Category</h5>
							
								  <input class="radiobtn" type="radio" name="Category" value="Laptop" checked> Laptop<br>
								  <input class="radiobtn" type="radio" name="Category" value="Mobile"> Mobile<br>
								  <input class="radiobtn" type="radio" name="Category" value="Tablet"> Tablet <br>
								  <input class="radiobtn" type="radio" name="Category" value="Accessories"> Accessories <br>
					</div>
					<div class="addproductoption">
						<h5>b)Add Product Name</h5>
						<input type="text" name="productName">
					</div>
					<div class="addproductoption">						
						<h5>c)Add Product Code</h5>
						<input type="text" name="productCode">
					</div>
					<div class="addproductoption">		
					    <h5>d)Add Price</h5>
						<input type="text" name="productPrice">
					</div>
					<div class="addproductoption">
						<h5>e)Add Details</h5>
						<textarea  cols="55" rows="5" name="productDetails"></textarea>
					</div>
					
						<button name="img_up" class="btn btn-success img_submit">Submit</button>
			
					</form> 
					</div>
				</div>
			<?php }else{
		echo '<h2  class="text-center">Access Denied!!!</h2>';
	}?>
				<?php include("websiteEndBody.php");?>






	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>


</body>
</html>
			