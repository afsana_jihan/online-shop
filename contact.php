<?php session_start(); ?>
<?php include("Database.php");?>
<?php
$databaseOb=new Database();
if(isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["comment"]) && isset($_POST["submit"])){
		$databaseOb->insertNewCommentToUserCommentTable($_POST["name"],$_POST["email"],$_POST["comment"]);
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
				<?php include("socialmanage.php");?>
			</div>
			<div class="container">
				<div class="row menu">
					<?php include("option.php");?>
					<?php include("addToCart.php");?>
				</div>
			</div>
		</section>
			<section class="breadcrums">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 brd-1">
					<h2>Contact Us</h2>
				</div>
			</div>
		
		</div>
		
		</section>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-offset-1 col-md-5 contactForm">
					<?php

					if(isset($_POST["submit"]) && isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["comment"])){
						echo "Thank you for contacting us!";
					}else  {
						?>
						<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
							<label>Name:</label>
							<br>
							<input type="text" name="name" placeholder="Write Your Name..">
							<br>
							<label>Email:</label>
							<br>
							<input type="text" name="email" placeholder="Write Your email..">
							<br>
							<label>Comment:</label>
							<br>
							<textarea  name="comment" rows="7" cols="60" ></textarea>
							<br>
							<br>
							<a href=""><button value="submit" name="submit" class="btn btn-success">Submit</button></a>
						</form>
						<?php
  }
?>
					</div>
					<div class="row">
					<div class="col-md-offset-1 col-md-5 contactDetails">
						<h3>Our Office</h3>
						<p><i class="fa fa-map-marker secondary2"></i> Address: Flat no B-5,99 Lalkhan Bazar,ctg</p>
						<p><i class="fa fa-phone"></i> Phone: 01521226962,0182345672</p>
						<p><i class="fa fa-envelope"></i> Email:<a href="https://www.gmail.com" > gadgethousehome@gmail.com</a></p>
					</div>
					</div>
				</div>
			</div>
		</section>
		
		
		
		<?php include("websiteEndBody.php");?>
			
			
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>

	

</body>
</html>