<?php session_start(); ?>
<?php include("Database.php");?>
<?php
$databaseOb=new Database();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
		if(isset($_POST["orderPId"])){
			$databaseOb->updateStatusInorderanddeliverytable($_POST["orderPId"]);
			
		}
	}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
			
			<?php include("socialmanage.php");?>
			</div>
			</section>
			<?php if(isset(($_SESSION['UserType']))&& $_SESSION['UserType']=='Admin'){?>
			
			<div class="row options">
				<?php include("admin_options.php");?>
			</div>
			<div class="container sm100 adminMain">
				<h2 class="text-center ">Hello Admin</h2>
				<div class="row registered">
					<div class="col-md-12">
			
			<h3><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo1" aria-expanded="false" aria-controls="collapseTwo ">Delivered Product</a></h3>
				<div class="answer collapse" id="collapseTwo1" role="tabpanel" aria-labelledby="headingTwo">
				
					
					<table border="2">
					
						<thead>
							<tr>
								<th>Sr</th>
								<th>Product Code</th>
								<th>Product Name</th>
								<th>Quantity</th>
								<th>Price</th>
								<th>Customer Name</th>
								<th>Phone Number</th>
								<th>Address</th>
							
								
							</tr>
						</thead>
						<tbody>
						<?php $i=1; $sum=0;$status=1;
						$allCartData=$databaseOb->getAllDataFromOrderAndDeliveryTable($status);
						while ($row = $allCartData->fetch_assoc()) {?>
													
							<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
							<tr>
								<td><?php echo $i++;?></td>
								<td><?php echo $row["ProductCode"];?> </td>
								<td><?php echo $row["ProductName"];?> </td>
								<td><?php echo $row["quantity"];?> </td>
								<td><?php $sum+=$row["totalprice"];echo $row["totalprice"];?> Tk</td>	
								<td><?php echo $row["CustomerName"];?> </td>
								<td><?php echo $row["CustomerNumber"];?> </td>
								<td><?php echo $row["CustomerAddress"];?> </td>
								
							</tr>
							
							</form>
						<?php } ?>
							
						</tbody>
						<tfoot>
							<tr>
								<td><?php echo $i++;?></td>
								<td>TOTAL</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>BDT</td>
								<td><?php echo $sum;?> Tk</td>
							</tr>
						</tfoot>
					
				
				</table>
			</div>
			</div>
			</div>
			</div>
			<?php }else{
		echo '<h2  class="text-center">Access Denied!!!</h2>';
	}?>
				<?php include("websiteEndBody.php");?>















	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>


</body>
</html>
			
