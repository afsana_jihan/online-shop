<?php session_start(); ?>
<?php include("Database.php");?>
<?php
$databaseOb=new Database();
$value="";
if(isset($_POST["search"]) &&isset($_POST["submit"])){
	$value=$_POST["search"];
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
				<?php include("socialmanage.php");?>
			</div>
			<div class="container">
				<div class="row menu">
					<?php if(isset(($_SESSION['UserType']))&& $_SESSION['UserType']=='Admin'){
						include("optionAdmin.php");
					}else{
						include("option.php");
					}?>
					
					<?php include("addToCart.php");?>
				</div>
			</div>
		</section>
			<section class="breadcrums">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 brd-1">
					<h2>Searched Products</h2>
				</div>
			</div>
		
		</div>
		
		</section>
		
		<section class="special-offer">
				<div class="container">
					<div class="row">
					<h2 class="text-center">Products</h2>
					<?php $allProduct=$databaseOb->getAllSearchProduct($value);
					$p=0;
					while ($row = $allProduct->fetch_assoc()) {$p=1;?>

						<form action="" method="">
						<div class="col-md-offset-1 col-md-2 offer-content">
							<a href="productDetails.php?productId=<?php echo $row["id"]?>"><img src="<?php echo $row["ProductImagePath"]?>" class="img-responsive"></a>
							<h6><?php echo $row["ProductName"]?></h6>
							<p>BDT <label><?php echo $row["ProductPrice"]?></label></p>
							<p>Available: <label><?php echo $databaseOb->AvailableNumberProduct($row["id"])?></label></p>
							<input type="hidden"value="<?php echo $row["id"]?>" name="productId">
							
							<input class="add-to-cart" type="submit" value="ADD To CART" name="addToCart">
						</div>
						</form>
					<?php } 
					if($p==0){
						echo "<h3>No product found!!!</h3>";
					}?>	
				
					</div>
				</div>
			</section>
		
			
			<?php include("websiteEndBody.php");?>
			
			
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
</body>
</html>