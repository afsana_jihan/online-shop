<?php session_start(); ?>
<?php include("Database.php"); ?>
<?php
$obDatabase = new Database();
$email="";
$hash="";
if(isset($_GET['email']) && isset($_GET['fp_code'])){
$_SESSION['Email']=$_GET['email'];
$email=$_SESSION['Email'];
$hash=$_GET['fp_code'];
}
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $password = $_POST["password"];
    $obDatabase->updatePasswordInUserTable($password, $_SESSION['Email']);
	unset($_SESSION['Email']);
	$_SESSION['Msg']="Password Reset Successfully";
    header('Location: login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GadgetHouse</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<section class="top-header">
		<div class="container-fluid">
			<?php include("logo.php");?>
			<?php if(isset($_SESSION['IS_LOGGED_IN'])){
			include("user_head.php");
			}else{
				include("head.php");
			}
			?>
				<?php include("socialmanage.php");?>

			</div>
			<div class="row st_login_form">
			<?php if ($obDatabase->userHashAndEmailCheck($email, $hash)) { ?>
		<div class="col-md-offset-3 col-md-6">
		
			<div class="login">
				<h3>Reset Password</h3>
			   <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                    <span>New-Password</span></br>
					<input type="password" id="password" name="password" placeholder="Your Password.." required><br>
					<span>Confirm Password</span></br>
					<input type="password" id="confirmPassword" name="confirmPassword" placeholder="Confirm Your Password.." required oninput="check(this)"><br>
					<a><button class="btn-success">Login</button></a>
                </form>
			</div>
		</div>
			<?php }
			else {
        ?>
        <h3 style="text-align: center"><?php echo "Link Error. Please try again by clicking forgot password from login option"; ?></h3>
        <?php
    }?>
</div>

					<?php include("websiteEndBody.php");?>



 










	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/jquery.magnific-popup.js"></script>
	<script src="assets/js/jquery.stellar.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>



<script language='javascript' type='text/javascript'>
                    function check(input) {
                        if (input.value != document.getElementById('password').value) {
                            input.setCustomValidity('Password Must be Matching.');
                        } else {
                            input.setCustomValidity('');
                        }
                    }
                </script>
				</body>
</html>